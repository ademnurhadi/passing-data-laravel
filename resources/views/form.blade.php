<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    <title>Halaman Sign In</title>
</head>

<body>
    <div class="container ">
        <h3 class="text-center pb-3 pt-3">Halaman Sign In</h3>
        <div class="card">
            <div class="card-body">
                <form action="/home" method="post">

                    @csrf
                    <div class=" mb-3">
                        <label for="Nama Depan" class="form-label">First Name</label>
                        <input type="text" class="form-control" name="namadepan">
                    </div>
                    <div class="mb-3">
                        <label for="Nama Depan" class="form-label">Last Name</label>
                        <input type="text" class="form-control" name="namabelakang">
                    </div>
                    <div class="form-check">
                        <h4>Gender:</h4>
                        <input class="form-check-input" type="radio" name="gender" id="flexRadioDefault1">
                        <label class="form-check-label" for="flexRadioDefault1">
                            male
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="gender" id="flexRadioDefault2" checked>
                        <label class="form-check-label" for="flexRadioDefault2">
                            Female
                        </label>
                    </div>
                    <select class="form-select mt-3" aria-label="Default select example">
                        <option selected>Open this select menu</option>
                        <option value="1">Indonesia</option>
                        <option value="2">Malaysia</option>
                        <option value="3">Singapore</option>
                    </select>
                    <div class="form-check pt-3">
                        <h4>Language Speaker</h4>
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">
                            English
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                        <label class="form-check-label" for="flexCheckChecked">
                            Arabic
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                        <label class="form-check-label" for="flexCheckChecked">
                            Dan lain lain
                        </label>
                    </div>

                    <div class="container mt-4">
                        <div class="form-group">
                            <label for="exampleTextarea">Input Bio</label>
                            <textarea name="bio" class="form-control" id="exampleTextarea" rows="3"></textarea>
                        </div>
                    </div>
                    <button type="submit" value="submit" class="btn btn-primary mt-4">Submit</button>
                </form>
            </div>
        </div>

    </div>



</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous">
</script>

</html>